# TV e-shop database app

Products database app based on .NET WinForms and DevExpress. 

## Features

- Main welcome screen with nav bar
- Database screen with two gridControls separated by splitter
- MS SQL Database
- Master-detail relation view in two separate grids

## Prerequisites

- Microsoft SQL Server Express with LocalDB feature installed and running locally on your machine

## Build

Run _Release/TVStoreApp.exe_<br/>

## Database
[TVProductsDatabase.mdf](Src/TVProductsDatabase.mdf)<br/>
![DatabaseStructure](Docs/DatabaseStructure.png)

## Screenshots
### Welcome screen
![WelcomeScreen](Docs/WelcomeScreen.png)<br/>
### Database screen with master-detail view
![GridsScreen](Docs/GridsScreen.png)
